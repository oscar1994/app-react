'use strict';

var multer = require('multer');
var multerConfig = require('../utils/conf');

module.exports = function (app) {
    var userController = require('../controllers/userController');

    app.route('/user')
        .get(userController.all_users)
        .post(multer(multerConfig.multerConf).single('photo'), userController.create_user);

    app.route('/user/:_id')
        .get(userController.user_by_id)
        .put(userController.update_user)
        .delete(userController.delete_user);
}