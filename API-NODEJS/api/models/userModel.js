'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    identification: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    birth_date: {
        type: String,
        required: true
    },
    photo: {
        type: String
        //required: true
    }
});

module.exports = mongoose.model('User', UserSchema);