'user strict';

var mongoose = require('mongoose'),
	Photo = mongoose.model('Photo');

exports.save_photo = function(req, res) {
	if (req.file) {
		req.body.photo = req.file.filename;
	}
	const photo = new Photo(req.body);
	photo.save(function(err, photo) {
		if (err) console.log(err);
		res.send(err);
		//res.json(photo);
	});
};

exports.photo_by_id = function(req, res) {
	Photo.findById(req.params.photo_id, function(err, photo) {
		if (err) res.send(err);
		res.json(photo);
	});
};

exports.update_photo = function(req, res) {
	Photo.findOneAndUpdate({ _id: req.params.photo_id }, req.body, { new: true }, function(err, photo) {
		if (err) res.send(err);
		res.json(photo);
	});
};

exports.delete_photo = function(req, res) {
	Photo.remove({ _id: req.params.photo_id }, function(err, photo) {
		if (err) res.send(err);
		res.json(photo);
	});
};
