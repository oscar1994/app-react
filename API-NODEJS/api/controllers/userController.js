'user strict';

var mongoose = require('mongoose'),
	User = mongoose.model('User');

exports.all_users = function (req, res) {
	User.find(function (err, user) {
		if (err) res.send(err);
		res.json(user);
	});
};

exports.create_user = function (req, res) {
	/*if (req.file) {
		req.body.photo = req.file.filename;
	}
	*/
	var new_user = new User(req.body);
	console.log(req.body);
	new_user.save(function (err, user) {
		if (err) res.send(err);
		res.json(user);
	});
};

exports.user_by_id = function (req, res) {
	User.findById(req.params.user_id, function (err, user) {
		if (err) res.send(err);
		res.json(user);
	});
};

exports.update_user = function (req, res) {
	console.log(req.params);
	User.findOneAndUpdate({ _id: req.params._id }, req.body, { new: true }, function (err, user) {
		if (err) res.send(err);
		res.json(user);
	});
};

exports.delete_user = function (req, res) {
	console.log(req.params._id);
	User.findByIdAndRemove(req.params._id, function (err, user) {
		if (err) res.send(err);
		res.json(user);
	});
};
