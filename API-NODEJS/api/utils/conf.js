'use strict';

var options = {
    database: 'mongodb://localhost:27017/react',
    port: process.env.PORT || 3001
};

var routes = {
    userRoute: './api/routes/userRoute'
};

exports.options = options;
exports.routes = routes;