var express = require('express'),
	app = express(),
	mongoose = require('mongoose'),
	User = require('./api/models/userModel'),
	bodyParser = require('body-parser');
var cors = require('cors');

app.use(cors());

var conf = require('./api/utils/conf');

var userRoute = require(conf.routes.userRoute);

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(conf.options.database, {
	useNewUrlParser: true
});

app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(bodyParser.json());

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'X-Requested-With');
	next();
});

userRoute(app);

app.listen(conf.options.port);

console.log('RESTful API started on: ' + conf.options.port);
