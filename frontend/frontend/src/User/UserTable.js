import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const styles = theme => ({
    root: {
        marginTop: theme.spacing.unit * 3,
        margin: '20px',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
});

const showUser = (user) => {
    console.log(user);
}

class UserTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: null
        };
        this.editUser = this.editUser.bind(this);
    }

    editUser = (user) => {
        console.log(user);
        this.props.user(user);
        this.props.showUserForm(true);
    }

    deleteUser(_id) {
        console.log(_id);

        fetch(`http://localhost:3001/user/${_id}`, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }            
        }).then(res => {
            return res.json();
        }).then(res => {
            console.log(res);
        });

    }

    render() {
        const { classes, users } = this.props;
        return (
            <div>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell align="center">Identification</CustomTableCell>
                                <CustomTableCell align="center">Name</CustomTableCell>
                                <CustomTableCell align="center">Last Name</CustomTableCell>
                                <CustomTableCell align="center">Birth Date</CustomTableCell>
                                <CustomTableCell align="center">Actions</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.map(user => (
                                <TableRow className={classes.row} key={user._id}>
                                    <CustomTableCell align="center">{user.identification}</CustomTableCell>
                                    <CustomTableCell align="center">{user.name}</CustomTableCell>
                                    <CustomTableCell align="center">{user.last_name}</CustomTableCell>
                                    <CustomTableCell align="center">{user.birth_date}</CustomTableCell>
                                    <CustomTableCell align="center">
                                        <IconButton onClick={() => { showUser(user) }} aria-label="info">
                                            <Icon>dehaze</Icon>
                                        </IconButton>
                                        <IconButton onClick={() => { this.editUser(user) }} aria-label="edit">
                                            <Icon>create</Icon>
                                        </IconButton>
                                        <IconButton onClick={() => { this.deleteUser(user._id) }} aria-label="delete">
                                            <Icon>delete_forever</Icon>
                                        </IconButton>
                                    </CustomTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            </div>
        )
    }
}

UserTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserTable);