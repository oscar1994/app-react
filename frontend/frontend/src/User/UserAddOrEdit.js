import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        'margin-left': '20px'
    },
    formControl: {
        margin: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class UserForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            _id: '',
            identification: '',
            name: '',
            last_name: '',
            birth_date: ''
        };
        this.userValues = this.userValues.bind(this);
        this.saveUser = this.saveUser.bind(this);
        this.handleShowUserForm = this.handleShowUserForm.bind(this);
        this.handleEditUser = this.handleEditUser.bind(this);
    }

    stateClean() {
        this.setState({
            _id: '',
            identification: '',
            name: '',
            last_name: '',
            birth_date: ''
        });
    }

    userValues(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    saveUser(event) {
        event.preventDefault();
        if (this.state._id !== '') {
            fetch(`http://localhost:3001/user/${this.state._id}`, {
                method: 'put',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "_id": this.state._id,
                    "identification": this.state.identification,
                    "name": this.state.name,
                    "last_name": this.state.last_name,
                    "birth_date": this.state.birth_date
                })
            }).then(res => {
                return res.json();
            }).then(res => {
                this.stateClean();
            });
        } else {
            fetch("http://localhost:3001/user", {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "identification": this.state.identification,
                    "name": this.state.name,
                    "last_name": this.state.last_name,
                    "birth_date": this.state.birth_date
                })
            }).then(res => {
                return res.json();
            }).then(res => {
                this.stateClean();
            });
        }
    }

    handleShowUserForm() {
        this.props.showUserForm(false);
        this.stateClean();
    }

    handleEditUser(user) {
        this.setState({
            _id: '',
            identification: '',
            name: '',
            last_name: '',
            birth_date: ''
        });
    }

    componentDidMount() {
        if (this.props.user == null) {
            this.stateClean();
        } else {
            this.setState({
                _id: this.props.user._id,
                identification: this.props.user.identification,
                name: this.props.user.name,
                last_name: this.props.user.last_name,
                birth_date: this.props.user.birth_date
            });
        }
    }

    render() {
        const { classes, user } = this.props;
        console.log(user);

        return (
            <div className={classes.container}>
                <form onSubmit={this.saveUser}>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="identification">Identification</InputLabel>
                        <Input value={this.state.identification} name="identification" id="identification" onChange={this.userValues} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="name">Name</InputLabel>
                        <Input value={this.state.name} name="name" id="name" onChange={this.userValues} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="last_name">Last Name</InputLabel>
                        <Input value={this.state.last_name} name="last_name" id="last_name" onChange={this.userValues} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="birth_date">Birth Date</InputLabel>
                        <Input value={this.state.birth_date} name="birth_date" id="birth_date" onChange={this.userValues} />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <Button onClick={this.saveUser} variant="outlined" color="primary" className={classes.button}>
                            Save
                        </Button>
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <Button onClick={this.handleShowUserForm} variant="outlined" color="secondary" className={classes.button}>
                            Cancel
                        </Button>
                    </FormControl>
                </form>
            </div>
        );
    }
}

UserForm.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserForm);