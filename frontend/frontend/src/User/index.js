import React, { Component } from 'react';
import UserTable from './UserTable';
import CircularIndeterminate from './CircularProgress';
import UserForm from './UserAddOrEdit';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';


class UserInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: null,
            user: null,
            showForm: false
        }
        this.showUserForm = this.showUserForm.bind(this);
    }

    getUsers = () => {
        fetch("http://localhost:3001/user").then(res => {
            return res.json();
        }).then(users => {
            this.setState({
                users: users
            });
        });
    }

    componentDidMount() {
        this.getUsers();
    }

    showUserForm = (value) => {
        this.setState({
            showForm: value
        });
    }

    handleUserToEdit = (value) => {
        this.setState({
            user: value
        });
    }

    render() {
        const { users, showForm, user } = this.state;
        return (
            <div>
                {
                    !showForm ? <IconButton onClick={this.showUserForm} aria-label="add"><Icon>add</Icon></IconButton> : null
                }
                {
                    showForm ? <UserForm showUserForm={this.showUserForm} user={user}></UserForm> : users ? <UserTable showUserForm={this.showUserForm} users={users} user={this.handleUserToEdit}></UserTable> : <CircularIndeterminate></CircularIndeterminate>
                }

            </div>
        );
    }
}

export default UserInfo;