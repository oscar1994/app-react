import React, { Component } from 'react';
import UserInfo from './User';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <h1>Users</h1>
          <UserInfo></UserInfo>
      </div>
    );
  }
}

export default App;
